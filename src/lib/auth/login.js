import { BASE_URL, URLS } from "$lib/utils";

// @ts-ignore
export async function handleLogin(credentials) {
  const params = {
    method: "POST",
    headers: {
      "content-type": "application/json",
    },
    body: JSON.stringify(credentials),
  };
  const response = await fetch(`${BASE_URL}/${URLS.auth.token}`, params);
  const json = await response.json();
  return json;
}
