export const BASE_URL = "http://localhost:8000";

export const URLS = {
  auth: {
    token: "api/v1/token/",
    refreshToken: "api/v1/token/refresh/",
  },
};

export const REDIRECTS = {
  organizations: {
    list: "/organizations",
    // @ts-ignore
    organization_space: (id) => `/organization/${id}`,
  },
};
